import { readFile, readdir } from "node:fs/promises";
import { basename, dirname, join, parse as pathParse } from "node:path";
import { fileURLToPath } from "node:url";
import { outputFile, remove } from "fs-extra";
import pascalCase from "just-pascal-case";
import { parse as svgParse, stringify } from "svgson";
import type { INode } from "svgson";

interface InputConfig {
  inputFolder: string;
  outputFolder: string;
  suffix: string;
  size: string;
}

interface OutputConfig {
  content: string;
  outputPath: string;
  exportPath: string;
  name: string;
}

const CONFIGS: InputConfig[] = [
  {
    inputFolder: join(
      dirname(fileURLToPath(import.meta.url)),
      "..",
      "heroicons/optimized/16/solid",
    ),
    outputFolder: join(dirname(fileURLToPath(import.meta.url)), "..", "src/components"),
    suffix: "16",
    size: "16px",
  },
];

const INDEX_FILE = join(dirname(fileURLToPath(import.meta.url)), "..", "src/index.ts");

async function processContent(content: string, size: string): Promise<string> {
  const parsedContent = await svgParse(content, {
    transformNode: (node) => {
      if (node.name === "svg") {
        const {
          attributes: { xmlns, viewBox, fill, "aria-hidden": ariaHidden },
        } = node;

        const processedNode: INode = {
          ...node,
          attributes: {
            xmlns,
            width: size,
            height: size,
            viewBox,
            fill,
            "aria-hidden": ariaHidden,
          },
        };

        return processedNode;
      }
      return node;
    },
  });

  return stringify(parsedContent);
}

async function getIcons(config: InputConfig): Promise<OutputConfig[]> {
  const { inputFolder, outputFolder, suffix, size } = config;

  const fileNames = await readdir(inputFolder);

  return Promise.all(
    fileNames.map<Promise<OutputConfig>>(async (fileName) => {
      const filePath = join(inputFolder, fileName);
      const content = await readFile(filePath, { encoding: "utf8" });
      const processedContent = await processContent(content, size);

      const componentFileName = `${pathParse(fileName).name}-${suffix}`;
      const componentFileNameExt = `${componentFileName}.tsx`;
      const componentName = pascalCase(componentFileName);

      const componentOutputPath = join(outputFolder, componentFileName, componentFileNameExt);
      const componentExportPath = `"./${join(
        basename(outputFolder),
        componentFileName,
        componentFileName,
      )}"`;

      const config = {
        content: processedContent,
        outputPath: componentOutputPath,
        exportPath: componentExportPath,
        name: componentName,
      };

      return config;
    }),
  );
}

function getComponent(name: string, content: string): string {
  const component = [
    'import { component$ } from "@builder.io/qwik";',
    "",
    `export const ${name} = component$(() => { return ${content} });`,
  ];

  return component.join("\n");
}

async function generateComponents(icons: OutputConfig[]): Promise<void> {
  await Promise.all(
    icons.map<Promise<void>>(async (icon) => {
      const { content, outputPath, name } = icon;
      const componentContent = getComponent(name, content);

      await outputFile(outputPath, componentContent);
    }),
  );
}

async function generateIndex(icons: OutputConfig[]): Promise<void> {
  const indexContent = icons
    .map((icon) => {
      const { name, exportPath } = icon;
      return `export { ${name} } from ${exportPath};`;
    })
    .join("\n");

  await outputFile(INDEX_FILE, indexContent);
}

async function main() {
  await remove(CONFIGS[0].outputFolder);

  const icons = await getIcons(CONFIGS[0]);

  await generateComponents(icons);
  await generateIndex(icons);
}

main();
