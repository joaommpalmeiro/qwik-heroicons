import { component$ } from "@builder.io/qwik";

export const Stop16 = component$(() => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16px"
      height="16px"
      viewBox="0 0 16 16"
      fill="currentColor"
      aria-hidden="true"
    >
      <rect width="10" height="10" x="3" y="3" rx="1.5" />
    </svg>
  );
});
