import { CopyButton } from "./copy-button";
import * as icons from "./index";

import "./root.css";

export default () => {
  return (
    <>
      <head>
        <meta charSet="utf-8" />
        <title>qwik-heroicons</title>
      </head>
      <body>
        <main>
          <ul class="grid">
            {
              // biome-ignore lint/style/useNamingConvention: `Icon` is a component and it must be capitalized in JSX
              Object.entries(icons).map(([iconName, Icon]) => {
                const importStatement = `import { ${iconName} } from "qwik-heroicons";`;
                return (
                  <li class="card">
                    <Icon />
                    <span>{iconName}</span>
                    <CopyButton toCopy={iconName}>Name</CopyButton>
                    <CopyButton toCopy={importStatement}>Import</CopyButton>
                  </li>
                );
              })
            }
          </ul>
        </main>
      </body>
    </>
  );
};
