import { Slot, component$, useSignal } from "@builder.io/qwik";
import copy from "clipboard-copy";

import { Check16 } from "./components/check-16/check-16";
import { Clipboard16 } from "./components/clipboard-16/clipboard-16";

interface CopyButtonProps {
  toCopy: string;
}

export const CopyButton = component$((props: CopyButtonProps) => {
  const copiedSig = useSignal(false);

  return (
    <button
      type="button"
      onClick$={async () => {
        await copy(props.toCopy);
        copiedSig.value = true;

        setTimeout(() => {
          copiedSig.value = false;
        }, 2000);
      }}
    >
      {copiedSig.value ? <Check16 /> : <Clipboard16 />}
      <Slot />
    </button>
  );
});
