# Notes

- https://www.npmjs.com/package/@qwikest/icons
- https://iconduck.com/
- https://github.com/yamatsum/nonicons/tree/master/src/icons/nonicons
- https://github.com/tailwindlabs/heroicons/blob/v2.1.1/scripts/build.js#L67
- https://github.com/tailwindlabs/heroicons/tree/v2.1.1/optimized
- https://github.com/elilabes/heroicons-qwik
- https://github.com/tailwindlabs/heroicons/tree/v2.1.1
- https://tailwindui.com/documentation#resources-icons
- https://github.com/joaopalmeiro/template-qwik-city-static
- https://github.com/qwikifiers/qwik-style-guide
- https://github.com/privatenumber/tsx
- https://github.com/BuilderIO/qwik/blob/v1.4.5/packages/qwik/src/cli/build/run-build-command.ts
- https://byby.dev/node-dirname-not-defined
- https://github.com/qwikest/icons/blob/v0.0.13/package.json#L192:
  - `"peerDependencies": { "@builder.io/qwik": ">=1.0.0" }`
- https://github.com/yisibl/resvg-js
- https://icones.js.org/collection/heroicons
- https://qwik.dev/api/qwik/#qwikintrinsicelements
- https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-hidden
- https://qwik.dev/docs/advanced/library/#packagejson:
  - "(...) `qwik` field, this is the entry point for the Qwik Optimizer, it will use this file to generate the `index.qwik.mjs` file."
- https://github.com/sindresorhus/type-fest/tree/main?tab=readme-ov-file#extending-existing-types
- https://github.com/sindresorhus/unicorn-magic/blob/main/default.js
- https://github.com/svgdotjs/svg.js
- https://github.com/svgdotjs/svgdom
- https://github.com/elrumordelaluz/svgson
- https://github.com/Rich-Harris/svg-parser
- https://github.com/mzusin/mz-svg
- https://stackoverflow.com/a/71132743
- https://legacy.reactjs.org/docs/jsx-in-depth.html#user-defined-components-must-be-capitalized: "User-Defined Components Must Be Capitalized"
- https://github.com/qwikifiers/qwik-ui/blob/%40qwik-ui/headless%400.2.2/apps/website/src/components/code-copy/code-copy.tsx
- https://qwik.dev/docs/components/overview/#inline-components
- https://github.com/qwikifiers/qwik-ui/blob/%40qwik-ui/headless%400.2.2/packages/kit-fluffy/src/components/button/button.tsx#L97
- https://flowbite.com/docs/components/clipboard/
- https://github.com/JustGoodUI/dante-astro-theme/blob/main/src/pages/blog/%5Bslug%5D.astro#L80
- https://github.com/antfu/icones/blob/23f29464dd4862aebf9cb5af3e6704528a71d1ae/src/utils/icons.ts#L82

## Commands

```bash
npm install -D @builder.io/qwik typescript vite vite-tsconfig-paths @biomejs/biome sort-package-json npm-run-all2 check-engine degit tsx just-pascal-case fs-extra @types/fs-extra type-fest svgson clipboard-copy rimraf
```

```bash
npm install -D "@types/node@$(cat .nvmrc | cut -d . -f 1-2)"
```

```bash
npm create qwik@latest
```

```bash
rm -rf lib/ lib-types/ tsconfig.tsbuildinfo
```

## Snippets

```ts
async function delay(seconds: number) {
  const duration = seconds * 1000;

  return new Promise((resolve) => {
    setTimeout(resolve, duration);
  });
}
```
