# qwik-heroicons

[Heroicons](https://heroicons.com/) icon components for [Qwik](https://qwik.dev/).

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npx degit github:tailwindlabs/heroicons#v2.1.1 heroicons
```

```bash
npm run build.icons
```

```bash
npm run format
```

```bash
npm run lint
```

```bash
npm run dev
```

```bash
npm run build
```

## Deployment

### Package

```bash
npm pack --dry-run
```

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [GitLab](https://gitlab.com/joaommpalmeiro/qwik-heroicons/-/tags).

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/qwik-heroicons).
